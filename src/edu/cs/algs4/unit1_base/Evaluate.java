package edu.cs.algs4.unit1_base;

import edu.princeton.cs.algs4.Stack;
import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

public class Evaluate {
    public static void main(String[] args) {
        //操作符栈
        Stack<String> ops = new Stack<String>();
        //操作数栈
        Stack<Double> vals = new Stack<Double>();

        /*
         * 读取输入字符串
         * 将操作数压入数据栈
         * 将操作符压入符号栈
         * 忽略左括号
         * 如果遇到右括号时，弹出操作数和一个操作符，进行运算，并把结果压入数据栈
         */
        while (!StdIn.isEmpty()) {

            String s = StdIn.readString();
            //如果为左括号则忽略
            if ( "(".equals(s) ) ;
            // 读取字符串，如果是运算符则压入ops栈
            else if ("+".equals(s)) ops.push(s);
            else if ("-".equals(s)) ops.push(s);
            else if ("*".equals(s)) ops.push(s);
            else if ("/".equals(s)) ops.push(s);
            else if ("sqrt".equals(s)) ops.push(s);
            //如果为右括号,弹出操作数和运算符，进行计算，并压入数据栈
            else if (")".equals(s)) {
                String op = ops.pop();
                Double val = vals.pop();
                if ("+".equals(op)) val = val + vals.pop();
                else if ("-".equals(op)) val = val + vals.pop();
                else if ("*".equals(op)) val = val + vals.pop();
                else if ("/".equals(op)) val = val + vals.pop();
                else if ("sqrt".equals(op)) val = Math.sqrt(val);
                vals.push(val);
            } else {
                vals.push(Double.parseDouble(s));
            }

            StdOut.println(vals.pop());
        }
    }
}
