package edu.cs.algs4.unit1_base;

import java.util.Iterator;

public class Stack<Item> implements Iterable<Item> {
    private Node first;

    private int size;

    private class Node {
        //定义了节点的嵌套类
        Item item;
        Node next;
    }

    public void push(Item item) {
        Node oldFirst = first;
        first = new Node();
        first.item = item;
        first.next = oldFirst;
        size ++;
    }

    public Item pop() {
        Item removeItem = first.item;
        first = first.next;
        size --;
        return removeItem;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int size() {
        return size;
    }

    @Override
    public Iterator<Item> iterator() {
        return new StackIterator();
    }

    private class StackIterator implements Iterator<Item> {

        private Node current = first;

        @Override
        public boolean hasNext() {
            return current != null ;
        }

        @Override
        public Item next() {
            Item item = current.item;
            current = current.next;
            return item;
        }

        @Override
        public void remove() {

        }
    }
}
