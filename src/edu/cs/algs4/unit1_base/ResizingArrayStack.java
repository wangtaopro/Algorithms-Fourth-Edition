package edu.cs.algs4.unit1_base;

import java.util.Iterator;

public class ResizingArrayStack<Item> implements Iterable<Item> {

    /**
     * 栈元素
     */
    private Item[] items = (Item[]) new Object[1];

    /**
     * 元素数量
     */
    private int size = 0;

    /**
     * 判断栈是否为空
     * @return 是-为空；否-不为空
     */
    public boolean isEmpty() {
        return this.size == 0;
    }

    /**
     * 获取栈的大小
     * @return
     */
    public int size() {
        return size;
    }

    /**
     * 重置数组大小为resize
     * @param resize
     */
    private void resize(int resize) {
        Item[] tempItems = (Item[]) new Object[resize];
        for (int i = 0 ; i < this.size ; i++) {
            tempItems[i] = this.items[i];
        }
        this.items = tempItems;
    }

    /**
     * 压入栈
     * @param item
     */
    public void push(Item item) {
        if (size == this.items.length) {
            resize(2*size);
        }
        this.items[size ++] = item;
    }

    /**
     * 删除栈顶元素
     * @return 被删除的栈顶元素
     */
    public Item pop() {
        Item item = this.items[-- size];
        this.items[size] = null;
        if (size > 0 && size == this.items.length / 4) {
            resize(size / 2);
        }
        return item;
    }

    public Iterator<Item> iterator() {
        return new ReverseArrayIterator();
    }

    private class ReverseArrayIterator implements Iterator<Item> {

        private int n = size;

        @Override
        public boolean hasNext() {
            return n > 0;
        }

        @Override
        public Item next() {
            return items[--n];
        }

        @Override
        public void remove() {

        }
    }


}
